<?php

namespace Comzero\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Comzero\AppBundle\Entity\Article;
use Comzero\AppBundle\Entity\BreakingNews;
use Comzero\AppBundle\Form\ArticleType;

/**
 * Article controller.
 *
 * @Route("/article")
 */
class ArticleController extends Controller
{
    /**
     * Lists all Article entities.
     *
     * @Route("/", name="article_single")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($slug = null)
    {
    	if($slug !== null) {
	        $em = $this->getDoctrine()->getManager();
	    	$article = $em->getRepository('ComzeroAppBundle:Article')->getSingleArticle($slug);
	
	        return $this->render('ComzeroAppBundle:Article:single.html.twig', array('article' => $article));
		} else {
			throw $this->createNotFoundException('Unable to find this article.');
		}
	}
	
	/**
     * Lists all entities in a category
     *
     * @Route("/category/{slug}", name="category")
     * @Method("GET")
     * @Template()
     */
	public function categoryAction($slug)
    {
    	$em = $this->getDoctrine()->getManager();
    	$categoryArticles = $em->getRepository('ComzeroAppBundle:Article')->getArticlesInCategory($slug);
		
        return $this->render('ComzeroAppBundle:Article:category.html.twig', array('categoryArticles' => $categoryArticles));
    }
	
    /**
     * Creates a new Article entity.
     *
     * @Route("/", name="article_create")
     * @Method("POST")
     * @Template("ComzeroAppBundle:Article:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity  = new Article();
        $form = $this->createForm(new ArticleType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('article_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Article entity.
     *
     * @Route("/new", name="article_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Article();
        $form   = $this->createForm(new ArticleType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Article entity.
     *
     * @Route("/{slug}", name="article")
     * @Method("GET")
     * @Template()
     */
    public function showAction($slug)
    {
        $em = $this->getDoctrine()->getManager();
    	$entity = $em->getRepository('ComzeroAppBundle:Article')->getSingleArticle($slug);
    	
    	if (!$entity) {
            throw $this->createNotFoundException('Unable to find this article.');
        }
        
        return $this->render('ComzeroAppBundle:Article:single.html.twig', array('article' => $entity));
    	
    	/*
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComzeroAppBundle:Article')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Article entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
        */
    }

    /**
     * Displays a form to edit an existing Article entity.
     *
     * @Route("/{id}/edit", name="article_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComzeroAppBundle:Article')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Article entity.');
        }

        $editForm = $this->createForm(new ArticleType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

     /**
     * Edits an existing Article entity.
     *
     * @Route("/breakingNews", name="breaking_news")
     * @Method("POST")

     */
    public function getBreakingNewsAction() {
			
		$em = $this->getDoctrine()->getManager();

        $messages = $em->getRepository('ComzeroAppBundle:BreakingNews')->getBreakingNews();
        
        if($messages !== false) {
        	$response = new JsonResponse(array("responseCode"=>200, "breakingNewsMessages"=>$messages));
		}
		else {
			$response = new JsonResponse(array("responseCode"=>400, "breakingNewsMessages"=>false));
		}
		
		return $response;
	}

    /**
     * Edits an existing Article entity.
     *
     * @Route("/{id}", name="article_update")
     * @Method("PUT")
     * @Template("ComzeroAppBundle:Article:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComzeroAppBundle:Article')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Article entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new ArticleType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('article_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Article entity.
     *
     * @Route("/{id}", name="article_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ComzeroAppBundle:Article')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Article entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('article'));
    }

    /**
     * Creates a form to delete a Article entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
