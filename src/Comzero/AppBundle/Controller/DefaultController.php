<?php

namespace Comzero\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name="Ross")
    {
        return $this->render('ComzeroAppBundle:Default:index.html.twig', array('name' => $name));
    }
}
