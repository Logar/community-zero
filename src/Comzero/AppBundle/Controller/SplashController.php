<?php

namespace Comzero\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Splash controller.
 *
 * @Route("/")
 */
class SplashController extends Controller
{
	/**
	 * Lists all Article entities.
	 *
	 * @Route("/", name="homepage")
	 * @Method("GET")
	 * @Template()
	 */
    public function indexAction()
    {
		$em = $this->getDoctrine()->getManager();
		
        $headlineArticles = $em->getRepository('ComzeroAppBundle:Article')->getHeadlineArticles();
		$videos = $em->getRepository('ComzeroAppBundle:Article')->getArticleVideos(5);
		$topCategoryArticles = $em->getRepository('ComzeroAppBundle:Article')->getTopCategoryArticles(5);
		
		//$topWriters = $em->getRepository('ComzeroAppBundle:Article')->getTopWriters(5);
		
        return $this->render('ComzeroAppBundle:Default:index.html.twig', array('headlineArticles' => $headlineArticles, 'videos'=> $videos, 'topCategoryArticles' => $topCategoryArticles));
    }
	
	/* function truncateText
	 * @param string $string
	 * @param integer $limit
	 * @param boolean $strict
	 */
	private function truncateText($string, $limit, $break = '.', $pad = '...', $strict = false) {
	
		// If the $string is shorter than the $limit, return the original source
		if (strlen($string) <= $limit) {
			return $string;
		}
		
		// If the string MUST be shorter than the $limit set.
		// Otherwise shorten to the first $break after the $limit
		if ($strict){
			$string = substr($string, 0, $limit);
			
			if (($breakpoint = strrpos($string, $break)) !== false){
				$string = substr($string, 0, $breakpoint).$pad;
			}
		}
		else{
			// If $break is present between $limit and the end of the string
			if (($breakpoint = strpos($string, $break, $limit)) !== false){
				if ($breakpoint < strlen($string) - 1)
				{
					$string = substr($string, 0, $breakpoint).$pad;
				}
			}
		}
		
		return $string;
	}

	// close opened html tags
    private function closeTags($html) {
        #put all opened tags into an array
        preg_match_all("#<([a-z]+)( .*)?(?!/)>#iU", $html, $result);
        $openedtags = $result[1];
        #put all closed tags into an array
        preg_match_all("#</([a-z]+)>#iU", $html, $result);
        $closedtags = $result[1];
        $len_opened = count($openedtags);
        # all tags are closed
        if(count ($closedtags) == $len_opened) {
			return $html;
        }
        $openedtags = array_reverse($openedtags);
        # close tags
        for( $i = 0; $i < $len_opened; $i++ ) {
            if (!in_array ($openedtags[$i], $closedtags)) {
				$html .= "</" . $openedtags[$i] . ">";
            }
            else {
				unset ($closedtags[array_search($openedtags[$i], $closedtags)]);
            }
        }
        return $html;
    }
}
