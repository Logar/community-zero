<?php

namespace Comzero\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\Response;

/**
 * Article
 * 
 * @author Ross Arena
 * @ORM\Entity(repositoryClass="Comzero\AppBundle\Repository\ArticleRepository")
 * @ORM\Table(name="articles")
 * @ORM\HasLifecycleCallbacks()
 */
class Article
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string", length=255)
     */
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=400)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string", length=255)
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="prelude", type="text")
     */
    private $prelude;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="youtube_video", type="string", length=255)
     */
    private $youtubeVideo;

    /**
     * @var string
     *
     * @ORM\Column(name="banner_image", type="string", length=500)
     */
    private $bannerImage;

    /**
     * @var string
     *
     * @ORM\Column(name="thumb_image", type="string", length=500)
     */
    private $thumbImage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="article_date", type="datetime")
     */
    private $articleDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_headline", type="smallint")
     */
    private $isHeadline;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set author
     *
     * @param string $author
     * @return Article
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Article
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set category
     *
     * @param string $category
     * @return Article
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Article
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set prelude
     *
     * @param string $prelude
     * @return Article
     */
    public function setPrelude($prelude)
    {
        $this->prelude = $prelude;

        return $this;
    }

    /**
     * Get prelude
     *
     * @return string 
     */
    public function getPrelude()
    {
        return $this->prelude;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Article
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set youtubeVideo
     *
     * @param string $youtubeVideo
     * @return Article
     */
    public function setYoutubeVideo($youtubeVideo)
    {
        $this->youtubeVideo = $youtubeVideo;

        return $this;
    }

    /**
     * Get youtubeVideo
     *
     * @return string 
     */
    public function getYoutubeVideo()
    {
        return $this->youtubeVideo;
    }

    /**
     * Set bannerImage
     *
     * @param string $bannerImage
     * @return Article
     */
    public function setBannerImage($bannerImage)
    {
        $this->bannerImage = $bannerImage;

        return $this;
    }

    /**
     * Get bannerImage
     *
     * @return string 
     */
    public function getBannerImage()
    {
        return $this->bannerImage;
    }

    /**
     * Set thumbImage
     *
     * @param string $thumbImage
     * @return Article
     */
    public function setThumbImage($thumbImage)
    {
        $this->thumbImage = $thumbImage;

        return $this;
    }

    /**
     * Get thumbImage
     *
     * @return string 
     */
    public function getThumbImage()
    {
        return $this->thumbImage;
    }

    /**
     * Set articleDate
     *
	 * @ORM\PrePersist
     * @param \DateTime $articleDate
     * @return Article
     */
    public function setArticleDate()
    {
    	if(!isset($this->articleDate))
        	$this->articleDate = new \DateTime();

        return $this;
    }

    /**
     * Get articleDate
     *
     * @return \DateTime 
     */
    public function getArticleDate()
    {
        return $this->articleDate;
    }

    /**
     * Set isHeadline
     *
     * @param integer $isHeadline
     * @return Article
     */
    public function setIsHeadline($isHeadline)
    {
        $this->isHeadline = $isHeadline;

        return $this;
    }

    /**
     * Get isHeadline
     *
     * @return integer 
     */
    public function getIsHeadline()
    {
        return $this->isHeadline;
    }
}
