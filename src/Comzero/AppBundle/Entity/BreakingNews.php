<?php

namespace Comzero\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BreakingNews
 *
 * @ORM\Table(name="breaking_news")
 * @ORM\Entity(repositoryClass="Comzero\AppBundle\Repository\BreakingNewsRepository")
 */
class BreakingNews
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=255)
     */
    private $message;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="message_date", type="datetime")
     */
    private $messageDate;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return BreakingNews
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set messageDate
     *
     * @param \DateTime $messageDate
     * @return BreakingNews
     */
    public function setMessageDate()
    {
        $this->messageDate = new \DateTime();

        return $this;
    }

    /**
     * Get messageDate
     *
     * @return \DateTime 
     */
    public function getMessageDate()
    {
        return $this->messageDate;
    }
}
