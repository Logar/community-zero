<?php

namespace Comzero\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class User
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="actual_name", type="string", length=255)
     */
    private $actualName;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=500)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="privileges", type="string", length=255)
     */
    private $privileges;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_status", type="integer")
     */
    private $userStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="profile_path", type="string", length=255)
     */
    private $profilePath;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="register_date", type="datetime")
     */
    private $registerDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last", type="datetime")
     */
    private $last;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set actualName
     *
     * @param string $actualName
     * @return User
     */
    public function setActualName($actualName)
    {
        $this->actualName = $actualName;

        return $this;
    }

    /**
     * Get actualName
     *
     * @return string 
     */
    public function getActualName()
    {
        return $this->actualName;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set privileges
     *
     * @param string $privileges
     * @return User
     */
    public function setPrivileges($privileges)
    {
        $this->privileges = $privileges;

        return $this;
    }

    /**
     * Get privileges
     *
     * @return string 
     */
    public function getPrivileges()
    {
        return $this->privileges;
    }

    /**
     * Set userStatus
     *
     * @param integer $userStatus
     * @return User
     */
    public function setUserStatus($userStatus)
    {
        $this->userStatus = $userStatus;

        return $this;
    }

    /**
     * Get userStatus
     *
     * @return integer 
     */
    public function getUserStatus()
    {
        return $this->userStatus;
    }

    /**
     * Set profilePath
     *
     * @param string $profilePath
     * @return User
     */
    public function setProfilePath($profilePath)
    {
        $this->profilePath = $profilePath;

        return $this;
    }

    /**
     * Get profilePath
     *
     * @return string 
     */
    public function getProfilePath()
    {
        return $this->profilePath;
    }

    /**
     * Set registerDate
     *
     * @return User
     */
    public function setRegisterDate()
    {
        $this->registerDate = new \DateTime();

        return $this;
    }

    /**
     * Get registerDate
     *
     * @return \DateTime 
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set last
     *
     * @return User
     */
    public function setLast()
    {
        $this->last = new \DateTime();

        return $this;
    }

    /**
     * Get last
     *
     * @return \DateTime 
     */
    public function getLast()
    {
        return $this->last;
    }
}
