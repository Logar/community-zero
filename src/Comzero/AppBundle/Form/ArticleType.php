<?php

namespace Comzero\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('author')
            ->add('slug')
            ->add('category')
            ->add('title')
            ->add('prelude')
            ->add('content')
            ->add('youtubeVideo')
            ->add('bannerImage')
            ->add('thumbImage')
            ->add('articleDate')
            ->add('isHeadline')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Comzero\AppBundle\Entity\Article'
        ));
    }

    public function getName()
    {
        return 'comzero_appbundle_articletype';
    }
}
