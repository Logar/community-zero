<?php

namespace Comzero\AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ArticleRepository
 *
 * @author Ross Arena
 */
class ArticleRepository extends EntityRepository
{
	public function getHeadlineArticles() {
		
		$qb = $this->_em->createQueryBuilder()
			->select('a')
			->from('ComzeroAppBundle:Article', 'a')
			->where('a.isHeadline = :first OR a.isHeadline = :second')
			->addOrderBy('a.isHeadline', 'DESC')
			->addOrderBy('a.articleDate', 'DESC')
			->setParameters(array('first' => 1, 'second' => 2));
			
		try {
			return $qb->getQuery()->getArrayResult();
		} catch (\Doctrine\ORM\NoResultException $e) {
			return false;
		}
	}
	
	public function getSingleArticle($slug) {

		$qb = $this->_em->createQueryBuilder()
			->select('a')
			->from('ComzeroAppBundle:Article', 'a')
			->where('a.slug = :slug')
			->setParameter('slug', $slug);
		
		try {
			return $qb->getQuery()->getSingleResult();
		} catch (\Doctrine\ORM\NoResultException $e) {
			return null;
		}
	    //return $qb->getQuery()->getSingleResult();
	}
	
	public function getArticlesInCategory($slug) {
	    
		$qb = $this->_em->createQueryBuilder()
			->select('a')
			->from('ComzeroAppBundle:Article', 'a')
			->where('a.category = :cat')
			->orderBy('a.articleDate', 'DESC')
			->setParameter('cat', $slug);
	 
	    return $qb->getQuery()->getArrayResult();
	}
	//SELECT * FROM (SELECT * FROM `articles` WHERE `is_headline`=0 
	//ORDER BY `article_date` DESC) AS a1 GROUP BY a1.`category` LIMIT $limit);
		
	public function getTopCategoryArticles($limit) {
	    
		$query = $this->_em->createQuery('SELECT a FROM ComzeroAppBundle:Article a WHERE a.isHeadline = :notHeadline 
				GROUP BY a.category ORDER BY a.articleDate DESC');
		$query->setParameters(array('notHeadline' => 0));
		/*
		$qb = $this->_em->createQueryBuilder()
			->select('a')
			->from('ComzeroAppBundle:Article', 'a')
			->where('a.isHeadline = :notHeadline')
			->groupBy('a.category')
			->addOrderBy('a.articleDate', 'DESC')
			->setMaxResults(':limit')
			->setParameters(array('notHeadline' => 0, 'limit' => $limit));
		*/
	    return $query->getResult();
	}
	
	public function getArticleVideos($limit) {
		
		$qb = $this->_em->createQueryBuilder()
			->select('a')
			->from('ComzeroAppBundle:Article', 'a')
			->where('a.youtubeVideo != \'\' AND a.youtubeVideo IS NOT NULL')
			->setMaxResults($limit);
			
	    return $qb->getQuery()->getArrayResult();
	}
	
}
