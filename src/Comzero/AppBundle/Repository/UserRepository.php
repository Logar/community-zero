<?php

namespace Comzero\AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * UserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserRepository extends EntityRepository
{
	public function getBreakingNews() {
	
		$qb = $this->_em->createQueryBuilder()
		->select('b')
		->from('ComzeroAppBundle:BreakingNews', 'b')
		->addOrderBy('b.messageDate', 'DESC')
		->setMaxResults(5);
	
		try {
			return $qb->getQuery()->getArrayResult();
		} catch (\Doctrine\ORM\NoResultException $e) {
			return false;
		}
	}
}
