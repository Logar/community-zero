<? 
	if(!isset($_SESSION)){ session_start(); }

	if(isset($_SESSION['member'])) { $uppercase_username = ucwords($_SESSION['member']); }
?>
<?=$header?>			
<body>

<? if(isset($_SESSION['SESS_ADMIN'])) { echo $adminBar; } ?>

	<div>
		<div class="float-left" style="width: 160px; height: 285px;">
			<?=$menu ?>
		</div>
		<div class="float-left"> <!--style="box-shadow: 8px 8px 5px #888888; width: 1100px; height: 600px;"-->
			<?=$content ?>
		</div>

		<div class="clear"></div>
	</div><div class="clear"></div>
</body>
</html>
