<? 
	if(!isset($_SESSION)){ session_start(); }
?>
<?=$header?>			
<body>
<? if(isset($_SESSION['SESS_ADMIN'])) { echo $adminBar; } ?>
	<div id="header-wrapper">
		<div class="container_12">
			<div class="grid_12">
				<div id="header">
					<div class="float-left"><a href="<?=SITE_URL?>" style="text-decoration: none;"><img src="<?=SITE_URL?>assets/images/com0-logo.png" alt="community zero logo" /></a></div>
					<div class="float-left" style="margin-top: 60px;">
						<div id="google_translate_element"></div>
						<script type="text/javascript">
							function googleTranslateElementInit() {
								new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-41763299-1'}, 'google_translate_element');
							}
						</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
					</div>
					<div class="float-right" style="margin-top: 20px;">
						<? 
							if(isset($user)) { if(!isset($_SESSION['SESS_ADMIN'])) { echo 'Welcome, <b>'.$user . '</b>&nbsp;|&nbsp; <a href="'.SITE_URL.'logout">Log Out</a>'; } }
							else { 
								//echo '<a href="'.SITE_URL.'login" id="login-link">Sign In</a>&nbsp;|&nbsp;<a href="'.SITE_URL.'register">Register</a>'; } 
								if(isset($login) && !isset($_SESSION['SESS_MEMBER'])) {
									echo $login;
									echo 'Or <a href="'.SITE_URL.'register">Sign up!</a>'; 
								}
							}	
						?>
						<!--<div class="float-left">
							<div id="searchbox">
								<form id="searchForm" name="searchbox" method="get" action="#" autocomplete="off">
									<input id="searchfield" name="blob" type="text" value="Search News &amp; Quotes" size="30" onfocus="var strSearchValue = document.forms['searchbox'].blob.value;if(strSearchValue.trim() == 'Search News &amp; Quotes'){document.forms['searchbox'].blob.value = '';}" onblur="var strSearchValue = document.forms['searchbox'].blob.value;if(strSearchValue.trim() == ''){document.forms['searchbox'].blob.value = 'Search News &amp; Quotes';}" onkeypress="if(event.keyCode == 13) {document.forms['searchbox'].submit();return false;}" maxlength="128" style="color: rgb(102, 102, 102);">
									<input type="submit" id="search-button" onclick="return Reuters.utils.submitSearch();" value="">
								</form>
							</div>
						</div>-->
					</div><div class="clear"></div>
				</div>
			</div>
		</div>
	</div><div class="clear"></div>

	<?=$menu ?>
	<div class="clear"></div>

	<div id="wrapper">
		<div class="container_12">
			<br />
			<div class="grid_12">
				<?=$content ?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<?=$footer ?>
</body>
</html>
