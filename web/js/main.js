$(function(){
	
	$('.delete').click(function(e) {
		e.preventDefault();
		var href = this.href;
		
		var answer = confirm("Are sure you want to delete this?");
		if (answer)
		{
			window.location.href = href;
		} else {
			return false;
		}
	});
	
	$('.single_comment_entity').hover(
	
		function() {
			var child = $(this).children('.comment-delete:first');
			child.show();
		},
		function() {
			var child = $(this).children('.comment-delete:first');
			child.hide();
		}
	);
	//Adjust height of overlay to fill screen when page loads
	$("#login-panel").css("height", $(window).height());

	//When the link that triggers the message is clicked fade in overlay/msgbox
	$("#login-link").click(function(e){
		e.preventDefault();
		$("#login-panel").fadeIn();
		return false;
	});
	
	//When the message box is closed, fade out
	$(".close").click(function(){
		$(this).closest(".dim").fadeOut();
		return false;
	});
	
	$("#register-form").submit(function(event) {
		
		// stop form from submitting normally
		event.preventDefault(); 
		var form = $(this), currUrl = form.attr('action');
		
		var dataString = $(this).serialize();
		$.ajax
		({
			type: "POST",
			url: currUrl,
			data: dataString,
			dataType: "json",
			cache: false,
			success: function(resp) {
				if(resp.status == 1) {
					console.log('got back');
					window.location.href = site_url;
				} else {
					$("#error-msg").empty().append(resp.msg);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
			}
		});
		
		return false;
	});
	

	function BreakingNews() {
		this.headlineMessages = new Array();
		this.gotBreakingNewsOnce = false;
		
		this.getBreakingNews = function(self) {
			
			$('#news-message').empty();
			
			var path = $('#breaking-news-path').attr('href');
			
			if(self.gotBreakingNewsOnce == false) {
				$.ajax
				({
					type: "POST",
					url: path,
					async: false,
					dataType: "json",
					contentType: 'application/json',
					cache: true,
					success: function(resp) {
						if(resp.responseCode == 200) {
							self.headlineMessages = resp.breakingNewsMessages;
							self.gotBreakingNewsOnce = true;
							
						} else if(resp.responseCode == 400) {
							console.log('failed to get headline messages');
						}
					},
					error: function(jqXHR, textStatus, errorThrown) {
						console.log(textStatus, errorThrown);
					}
				});
			}
		};
		
		this.iterateBreakingNews = function (target, message, index, interval, dfd) {
	        if (index < message.length) {
	            $(target).append(message[index++]);
	            setTimeout(this.iterateBreakingNews.bind(this, target, message, index, interval, dfd), interval);
	        } else {
	            dfd.resolve();
	        }
	    };
	}

	BreakingNews.prototype.showBreakingNews = function (ele, index) {

	    var returnedVal = false;
	    
	    if (typeof this.headlineMessages[index] !== 'undefined' && this.headlineMessages[index] !== null) {

	        var self = this;
	        
	        // Create a deferred object
	        var defer = $.Deferred();
	        
	        self.iterateBreakingNews(ele, self.headlineMessages[index].message, 0, 50, defer);
	            
	        defer.done(function() {
				setTimeout(function() {
					$('#news-message').empty();
					
					index = index + 1;
					
					self.showBreakingNews("#news-message", index);
				}, 6000); 
	        });

	    } else {
	        return this.showBreakingNews("#news-message", 0);
	    }
	};
	
	var breakingNewsObject = new BreakingNews;
	
	breakingNewsObject.getBreakingNews(breakingNewsObject);
	breakingNewsObject.showBreakingNews("#news-message", 0);
	
	$('.go-top').click(function() {
		$('html, body').animate({scrollTop:0}, 0);
		return false;
	});
	
	
	$('#signin-link').hover( 
		function () {
			$('#signin-buttons-container').show();
		}, 
		function () {
			$('#signin-buttons-container').hide();
		}
	);
	
});